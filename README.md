# Consul-config

Demonstration using git2consul synchronising configuration stored in a git repository into a consul cluster.

## Benefits of Consul
* Language agnostic as it has various bindings and a simple API to push/pull values
* Other capabilities such as Service Discovery and DNS are also useful - reuse
* Reliable

# Usage

Create or update any config file under `config/` and the pipeline will do the rest on a push of a new commit(s).

# Setup

[Git2Consul](https://github.com/breser/git2consul) is installed in a docker container gitlab-runner along with dependencies NodeJS and NPM.
The configuration file in the repository `git2consul.json` defines the settings for git2consul to use such as:
* The git repository to pull
* Whether to use filename extensions, folders in the consul key names.
* which branch and/or sub directory in the git repository to process into Consul.

# Notes
Git2Consul downloads the git repository in the gitlab-runner in `/tmp`
The `gitlab-runner` user needs read and write access to `/tmp` in the gitlab-runner.

# Future work
* Make Gitlab use 1 pipeline for git2consul process and trigger from the application config repos - [GitLab pipeline documentation](https://docs.gitlab.com/ee/ci/triggers/README.html)